## Game Deals

**Autor**: Alejandro Jose Alvez Farias

### La puedes probar en: http://alvez.pythonanywhere.com

### Librerías y frameworks utilizados:

- **Flask**: microframework para crear aplicaciones web, está basado en Werkzeug y Jinja 2.
- **Requests**: librería utilizada para realizar peticiones HTTP.
- **Urllib**: librería que te permite parsear URLs.

### Descripción breve:

Aplicación web hecha con python que busca ofertas de videojuegos de PC utilizando la API de https://isthereanydeal.com
