from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for
)
from werkzeug.exceptions import abort

import requests
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
import urllib.parse

bp = Blueprint('search', __name__)

@bp.route('/search')
def search():
    if request.method == 'GET':
        query = request.args.get('q')

        game = urllib.parse.quote(query)
        url = "https://api.isthereanydeal.com/v01/search/search/?key=f3128cf8c6600c43e18e75d8bfc824e3d5da2be1&q=" + game + "&offset=0&limit=20&region=eu2&country=es"

        results = requests.get(url).json()

        deals = []

        for result in results['data']['list']:
            deal = Deal()

            deal.game = result['title']
            deal.new = result['price_new']
            deal.old = result['price_old']
            deal.cut = result['price_cut']
            deal.shop = result['shop']['name']
            deal.drms = result['drm']
            deal.urlbuy = result['urls']['buy']
            deal.urlinfo = result['urls']['game']

            deals.append(deal)                    

        return render_template('search.html', deals=deals, query=query)

class Deal:
    def __init__(self):
        self._game = ''
        self._new = 0
        self._old = 0
        self._cut = 0
        self._shop = ''
        self._drms = ''
        self._urlbuy = ''
        self._urlinfo = ''

    @property
    def game(self):
        return self._game

    @game.setter
    def game(self, g):
        self._game = g

    @property
    def new(self):
        return self._new

    @new.setter
    def new(self, h):
        self._new = h

    @property
    def old(self):
        return self._old

    @old.setter
    def old(self, h):
        self._old = h

    @property
    def cut(self):
        return self._cut

    @cut.setter
    def cut(self, h):
        self._cut = h

    @property
    def shop(self):
        return self._shop

    @shop.setter
    def shop(self, h):
        self._shop = h

    @property
    def drm(self):
        return self._drm

    @drm.setter
    def drm(self, h):
        self._drm = h

    @property
    def urlbuy(self):
        return self._urlbuy

    @urlbuy.setter
    def urlbuy(self, h):
        self._urlbuy = h

    @property
    def urlinfo(self):
        return self._urlinfo

    @urlinfo.setter
    def urlinfo(self, h):
        self._urlinfo = h
